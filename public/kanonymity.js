function clearAll() {
    var lists = document.getElementsByTagName('ul');
    for (let list of lists) {
        while (list.hasChildNodes()) {
            list.removeChild(list.lastChild);
        }
    }
}

export function process_csv(csv, width, length) {
    let list = csv.split('\\n');
    if (list.length != length)
        throw 'CSV incompatible with provided format (length)'

    let data = []
    for (let i=0; i < length; i++) {
        let data_row = list[i].split(',')
        for (let i = 0; i < data_row.length; i ++ ) { 
            data_row[i] = data_row[i].trim()
        }
        if (data_row.length != width)
            throw 'CSV incompatible with provided format (width): ' + data_row
        data.push(data_row)
    }
    return data
}

function normalise(row) {
    // Skip the first element, which is the id, and the last element, which is the disease
    let n = ""
    for (var i=1; i < row.length - 1; i++) {
        n += row[i]
    }
    return n.replace(/\//g, "");
}


export function addClass(id, id_out, data2) {
    let classes = new Object ();
    let reverse_classes  = []
    let current_class = 0;

    let table = document.getElementById(id);

    let rows = table.rows
    table.tHead.children[0].appendChild(document.createElement("th")).innerText = "Class";

    // var cell = rows[0].insertCell(rows[0].length);
    // cell.innerText = "Class"

    for (var i = 1; i < rows.length; i++) {
        let row = rows[i]
        let cell = row.insertCell(row.length);
        let normal_value = normalise(data2[i])
        let class_value = -1;

        if (classes[normal_value] == null) {
            classes[normal_value] = current_class;
            reverse_classes[current_class] = [i];
            current_class += 1;
        } else {
            reverse_classes[classes[normal_value]].push(i)
        }

        class_value = classes[normal_value]
        // cell.innerText = class_value + "(" + normal_value + ")"
        cell.innerText = class_value
    }

    let k = data2.length - 1;
    let k_c = 0;
    for (var i  = 0; i < reverse_classes.length; i++ ) {
        if (reverse_classes[i].length < k) {
            k = reverse_classes[i].length
            k_c = i
        }

    }

    var output = document.getElementById(id_out)
    var h = document.createElement("b")                // Create a <h1> element
    var t = document.createTextNode("The K-anonymity of the table is " + k + " (example of mininal class: " + k_c + ")");     // Create a text node
    h.appendChild(t);                                   // Append the text to <h1>
    output.appendChild(h)
}


export function createRow(id, data) {
    let table = document.getElementById(id);
    let header = table.createTHead();
    let body = table.appendChild(document.createElement('tbody'));
    let width = data[0].length
    // First line are the headers
    var newRow  = header.insertRow();
    for (var i = 0 ; i < width; i++) {
        let cell = table.tHead.children[0].appendChild(document.createElement("th"));
        cell.innerText = data[0][i];
    }

    for (var j = 1 ; j < data.length; j++){
        var newRow = body.insertRow();
        for (var i = 0 ; i < width; i++) {
            var cell = newRow.insertCell(i);
            cell.innerText = data[j][i];
        }

    }
}

function replaceAt(str, start, stop, sub) {
    return str.substr(0, start) + sub + str.substr(stop)
}

function hideDayDoB(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][1] = replaceAt(data2[i][1], 0, 2, "**")
    }
}

function hideMonthDoB(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][1] = replaceAt(data2[i][1], 3, 5, "**")
    }
}

function hideYearDoB(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][1] = replaceAt(data2[i][1], 6, 10, "****")
    }
}

function hideGender(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][3] = "*";
    }
}

function hideLastDigitZIP(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][2] = replaceAt(data2[i][2], 4, 5, "*")
    }
}

function hideLastTwoDigitZIP(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][2] = replaceAt(data2[i][2], 3, 5, "**")
    }
}

function hideLastThreeDigitZIP(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][2] = replaceAt(data2[i][2], 2, 5, "***")
    }
}

function hideLastFourDigitZIP(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][2] = replaceAt(data2[i][2], 1, 5, "****")
    }
}

function hideLastFiveDigitZIP(data2) {
    for (var i=1 ; i < data2.length; i ++) {
        data2[i][2] = replaceAt(data2[i][2], 0, 5, "*****")
    }
}

function computePrivacy(id, id_out, data_csv, width, length){
    let data2 = JSON.parse(JSON.stringify(process_csv(data_csv, width, length)));
    const myNode = document.getElementById(id);
    myNode.innerHTML = '';

    var output = document.getElementById(id_out)
    output.innerHTML = ''

    if (document.getElementById("hideGender").checked) {
        hideGender(data2);
    }

    if (document.getElementById("hideDayDoB").checked)
        hideDayDoB(data2);

    if (document.getElementById("hideMonthDoB").checked)
        hideMonthDoB(data2);

    if (document.getElementById("hideYearDoB").checked)
        hideYearDoB(data2);

    if (document.getElementById("hideLastDigitZIP").checked)
        hideLastDigitZIP(data2);

    if (document.getElementById("hideLastTwoDigitZIP").checked)
        hideLastTwoDigitZIP(data2);

    if (document.getElementById("hideLastThreeDigitZIP").checked)
        hideLastThreeDigitZIP(data2);

    if (document.getElementById("hideLastFourDigitZIP").checked)
        hideLastFourDigitZIP(data2);

    if (document.getElementById("hideLastFiveDigitZIP").checked)
        hideLastFiveDigitZIP(data2);

    //
    createRow(id, data2, width, length);
    addClass(id, id_out, data2);

}
